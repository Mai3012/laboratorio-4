class MonstruosController < ApplicationController
  
  def index
    @monstruo=Monstruo.all
  end

  def show
  	#punto1
  	@monstruo=Monstruo.find(params[:id])
    #punto5
    if @monstruo.tweets.count <1 then 
      redirect_to monstruos_path, notice: 'no tiene tweets'
    end
  end

  def edit    
  end
  
  def destroy
  	#punto3
  	monstruo= Monstruo.find(params[:id])
  	monstruo.destroy
  	redirect_to monstruos_path, notice: 'se borro correctamente'
  end
end
